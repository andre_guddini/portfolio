const buttons = document.querySelectorAll('.project-button');
const closeBtn = document.querySelectorAll('.modal-button');
// Attach click event listeners to each button
buttons.forEach(button => {
    button.addEventListener('click', function () {
        const modalId = this.getAttribute('data-modal-target');
        const modal = document.getElementById(modalId);
        modal.style.display = 'block';

        modal.addEventListener('click', function (event) {
            if (event.target === modal) {
                modal.style.display = 'none';
            }
        });
        closeBtn.forEach(button => {
            button.addEventListener('click', function () {
                modal.style.display = 'none';
            })
        })

    });
});


// mobile devices project info
const projectContainers = document.querySelectorAll(".project-container");

projectContainers.forEach(container => {
    const projectInfo = container.querySelector(".project-info");

    container.addEventListener("click", function () {
        projectInfo.classList.toggle("show");
    });
})

gsap.fromTo("#andrew", { opacity: 0, y: '-200' }, { opacity: 1, y: '0', duration: 1, ease: 'power1' });
gsap.fromTo("#front-end-dev", { opacity: 0, y: '200' }, { opacity: 1, y: '0', duration: 1, ease: 'power1' });
gsap.fromTo(".nav-list li", { opacity: 0, x: '200' }, { opacity: 1, x: '0', duration: 1, ease: 'power1', stagger: '0.5' });
gsap.fromTo(".tech-section h2", { opacity: 0, y: '200' }, { opacity: 1, y: '0', duration: 1, ease: 'power1' });
gsap.fromTo(".tech-section li", { opacity: 0, y: '200' }, { opacity: 1, y: '0', duration: 1, ease: 'power1', stagger: '0.5', delay: '0.5' });

gsap.fromTo(".projects-description", {
    y: '-50%',
    x: '-50%',
}, {
    y: 0,
    x: '0',
    duration: 1,
    ease: 'power1',
    scrollTrigger: {
        trigger: ".projects-description",
        start: "top 100%",
        end: "bottom 20%",
        scrub: true,
        //   markers: true 
    }
})

gsap.fromTo(".contact-description", {
    scale: 1
}, { scale: 1.2, duration: 1, yoyo: true, repeat: -1 });
